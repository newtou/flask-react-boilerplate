import React from 'react';
import history from './AppHistory';
import { Router, Route, Switch } from 'react-router-dom';

// components
import App from '../App';

function AppRoutes() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={App}></Route>
      </Switch>
    </Router>
  );
}

export default AppRoutes;
