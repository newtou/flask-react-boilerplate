import os
from flask_cors import CORS
from dotenv import load_dotenv
from os.path import exists, join
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager
from flask import Flask, send_from_directory

# load env variables
load_dotenv(verbose=True)

# env variables
mongo_uri = os.getenv('MONGO_URI')
jwt_secret = os.getenv('JWT_SECRET')


# assign modules to variables
cors = CORS()
db = MongoEngine()
jwt = JWTManager()


def create_app():
    # initialise app
    app = Flask(__name__, static_folder='../client/build')

    # app config
    app.config.update = (
        SECRET_KEY=os.urandom(64)
        JWT_SECRET_KET=jwt_secret
    )
    # mongodb config
    app.config['MONGODB_SETTINGS'] = {
        'db': '',
        'host': mongo_uri,
        'connect': False
    }

    # initialise modules
    cors.init_app(app)
    db.init_app(app)
    jwt.init_app(app)

    # run react static files
    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def catch_all(path):
        file_to_serve = path if path and exists(
            join(app.static_folder, path)) else 'index.html'
        return send_from_directory(app.static_folder, file_to_serve)

    # import blueprints & register views
    from .views import api
    app.register_blueprint(api)

    return app
